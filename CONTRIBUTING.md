# UpDevTools Contributing Guide

:+1::tada: WooHoo, thank you for taking the time to contribute! You rock! :tada::+1:

We welcome all contributions including feedback, bug reports, and pull requests.  Have an idea for a tool or feature? Awesome. Jump in and contribute.

#### Table Of Contents

[How Can I Contribute?](#how-can-i-contribute)
  * [Reporting Bugs](#reporting-bugs)
  * [Feature Requests](#feature-requests)
  * [Merge - Pull Requests](#merge-requests)

## How Can I Contribute?

There are many ways to contribute. This suite of tools is for you. It's up to all of us to contribute in order to make it the development suite we all want and need.
  
### Reporting Bugs

Whoopsie, did you find a bug? Here's what you want to do. Open up our [issue tracker](https://gitlab.com/uptechlabs/updevtools/issues).  Then submit a new issue and mark it with a `bug` label.  We use the labels to identify the different types of tasks, including buggies.

### Feature Requests

Have an idea for a new feature or toolset?  Awesome.  Your fellow developers will love you.  

To create a feature request, just open an issue on our [issue tracker](https://gitlab.com/uptechlabs/updevtools/issues).  When creating the issue, make sure that you mark the label as `feature request`.  We use the labels to identify the different types of tasks, as our issue tracker is comprised of issues and tasks.

### Merge - Pull Requests

If you are familiar with pull requests from GitHub, then you're ready to start with the GitLab's [Merge Requests](https://gitlab.com/uptechlabs/updevtools/merge_requests).